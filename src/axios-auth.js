import axios from 'axios'

const instance = axios.create({
  baseURL: 'https://api-rest-d2488.firebaseio.com'
})

// instance.defaults.headers.common['SOMETHING'] = 'something'

export default instance